package ZaSnake;

import ZaSnake.db.OperatiiDB;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class HighScoreController implements Initializable {

    @FXML
    public Button btnBackMainFromHighScore;

    @FXML public TableView<Player> table;
    @FXML public TableColumn<Player, String> playerName;
    @FXML public TableColumn<Player, Integer> score;

    @FXML
    private AnchorPane anchorRoot;

    @FXML
    private StackPane parentContainer;


    public void openMyLink(MouseEvent mouseEvent) {
        openWebpage("https://gitlab.com/users/Bremus");
    }

    public static void openWebpage(String url) {
        try {
            Desktop.getDesktop().browse(new URL(url).toURI());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void openLinkTrainer(MouseEvent mouseEvent) {
        openWebpage("https://gitlab.com/sda-international/users/courses/romania/javagalati1/javagalati1/gabriel_mototolea");
    }

    public void exitHighScore(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(new File("src/main/resources/MainMenu.fxml").toURI().toURL());
        Scene scene = anchorRoot.getScene();
        //Set Y of second scene to Height of window
        root.translateYProperty().set(scene.getHeight());
        //Add second scene. Now both first and second scene is present
        parentContainer.getChildren().add(root);

        //Create new TimeLine animation
        Timeline timeline = PlayerTimeLine.getTimeline();
        //Animate Y property
        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
        KeyFrame kf = new KeyFrame(Duration.millis(0.1), kv);
        timeline.getKeyFrames().add(kf);
        //After completing animation, remove first scene
        timeline.setOnFinished(t -> parentContainer.getChildren().remove(anchorRoot));
        timeline.play();
    }

    public void openLinkColleague(MouseEvent mouseEvent) {
        openWebpage("https://gitlab.com/Val3nt1n");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        final List<Player> playerDetails = OperatiiDB.getPlayerDetails();
        System.out.println(playerDetails);
        loadColumns();
        table.setItems(FXCollections.observableList(playerDetails));
    }
    private void loadColumns() {
        playerName.setCellValueFactory(new PropertyValueFactory<>("name"));
        score.setCellValueFactory(new PropertyValueFactory<>("score"));
    }
}