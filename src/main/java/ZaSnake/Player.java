package ZaSnake;

import javax.persistence.*;
import java.io.Serializable;
import java.util.StringJoiner;

@Entity
@Table(schema = "snake", name = "player")
@NamedQueries(
        @NamedQuery(name = "getPlayersORderedByScore", query = "from Player order by score desc")
)
//@NamedQuery(name = "getName", query = Player.getPlayerByName)
//@NamedQuery(name = "updateScore", query = Player.updateScorePlayer)
public class Player implements Serializable {
    //  static final String getPlayerByName = "from Player d where d.name = :name";

    //  static final String updateScorePlayer = "UPDATE Player d SET d.score=:score where d.name = :name";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private String name;

    @Column
    private Integer score;

    public Player() {
    }

    public Player(String name, Integer score) {
        this.name = name;
        this.score = score;
    }

    public Player(Long id, String name, Integer score) {
        this.id = id;
        this.name = name;
        this.score = score;
    }

    public Player(String name) {

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

//    @Override
//    public String toString() {
//        return "Player Name : " + name;
//    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Player.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("score=" + score)
                .toString();
    }
}