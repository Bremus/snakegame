package ZaSnake;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TheGameController implements Initializable {

    public AnchorPane gamePane;
    @FXML
    private StackPane parentContainer;

    @FXML
    private AnchorPane anchorRoot;

    @FXML
    public Label playerNameLabel;


    public void setPlayerName(String message) {
        playerNameLabel.setText(message);

    }


    public void openResumeJOC(KeyEvent keyEvent) throws IOException {
        if (keyEvent.getCode() == KeyCode.ESCAPE) {
            PlayerTimeLine.setPlayerName(playerNameLabel.getText());

            Parent root = FXMLLoader.load(new File("src/main/resources/ResumeTheGame.fxml").toURI().toURL());
            Scene scene = anchorRoot.getScene();

            parentContainer.getChildren().add(root);
            //Create new TimeLine animation
            Timeline timeline = PlayerTimeLine.getTimeline();
            //Animate Y property
            KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
            KeyFrame kf = new KeyFrame(Duration.millis(0.1), kv);
            timeline.getKeyFrames().add(kf);
            //After completing animation, remove first scene
            timeline.setOnFinished(t -> parentContainer.getChildren().remove(anchorRoot));
            timeline.play();
        }
    }

    private static final int WIDTH = 800;
    private static final int HEIGHT = 550;
    private GameLoop loop;
    private Grid grid;
    private GraphicsContext context;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Canvas canvas = new Canvas(WIDTH, HEIGHT);
        context = canvas.getGraphicsContext2D();
        canvas.setFocusTraversable(true);
        canvas.setOnKeyPressed(e -> {
            Snake snake = grid.getSnake();
            if (loop.isKeyPressed()) {
                return;
            }
            switch (e.getCode()) {
                case UP:
                case NUMPAD8:
                case W:
                    snake.setUp();
                    break;
                case DOWN:
                case NUMPAD2:
                case S:
                    snake.setDown();
                    break;
                case LEFT:
                case NUMPAD4:
                case A:
                    snake.setLeft();
                    break;
                case RIGHT:
                case NUMPAD6:
                case D:
                    snake.setRight();
                    break;
                case ENTER:
                    if (loop.isPaused()) {
                        reset();
                        (new Thread(loop)).start();
                    }
            }
        });
        reset();

        gamePane.getChildren().add(canvas);
        (new Thread(loop)).start();
    }

    private void reset() {
        grid = new Grid(WIDTH, HEIGHT);
        loop = new GameLoop(grid, context);
        Painter.paint(grid, context);
    }
}