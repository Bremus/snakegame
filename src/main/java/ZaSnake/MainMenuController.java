package ZaSnake;

import javafx.animation.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.util.Duration;


import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;


public class MainMenuController implements Initializable{

    @FXML private AnchorPane anchorRoot;
    @FXML private StackPane parentContainer;
    @FXML private Button btnStart = new Button();
    @FXML private Button btnHighScore = new Button();
    @FXML private Button btnExit = new Button();

    private Timeline timeline;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        timeline = new Timeline();
        PlayerTimeLine.setTimeline(timeline);
    }

    @FXML
    private void loadSecondScene(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(new File("src/main/resources/NameMenu.fxml").toURI().toURL());
        Scene scene = anchorRoot.getScene();
        //Set Y of second scene to Height of window
        root.translateYProperty().set(scene.getHeight());
        //Add second scene. Now both first and second scene is present
        parentContainer.getChildren().add(root);

        //Create new TimeLine animation
        Timeline timeline = PlayerTimeLine.getTimeline();
        //Animate Y property
        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
        KeyFrame kf = new KeyFrame(Duration.millis(0.1), kv);
        timeline.getKeyFrames().add(kf);
        //After completing animation, remove first scene
        timeline.setOnFinished(t -> {
            parentContainer.getChildren().remove(anchorRoot);
        });
        timeline.play();
    }

    public void onClickExit(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void openMyLink(MouseEvent mouseEvent) {
        openWebpage("https://gitlab.com/users/Bremus");
    }

    public static void openWebpage(String url) {
        try {
            Desktop.getDesktop().browse(new URL(url).toURI());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void openLinkTrainer(MouseEvent mouseEvent) {
        openWebpage("https://gitlab.com/sda-international/users/courses/romania/javagalati1/javagalati1/gabriel_mototolea");
    }

    public void showHighScore(ActionEvent actionEvent) throws IOException {

        Parent root = FXMLLoader.load(new File("src/main/resources/HighScore.fxml").toURI().toURL());
        Scene scene = anchorRoot.getScene();
        //Set Y of second scene to Height of window
        root.translateYProperty().set(scene.getHeight());
        //Add second scene. Now both first and second scene is present
        parentContainer.getChildren().add(root);

        //Create new TimeLine animation
        Timeline timeline = PlayerTimeLine.getTimeline();
        //        //Animate Y property
        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
        KeyFrame kf = new KeyFrame(Duration.millis(0.1), kv);
        timeline.getKeyFrames().add(kf);
        //After completing animation, remove first scene
        timeline.setOnFinished(t -> parentContainer.getChildren().remove(anchorRoot));
        timeline.play();
    }

    public void openLinkColleague(MouseEvent mouseEvent) {
        openWebpage("https://gitlab.com/Val3nt1n");
    }


}