package ZaSnake;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

public class GameOverController implements Initializable {

    @FXML
    private StackPane parentContainer;

    @FXML
    private AnchorPane anchorRoot;

    @FXML
    Button btnHighScore = new Button();

    @FXML
    Button btnQuitToMainMenu = new Button();

    @FXML
    Button btnExit = new Button();

    @FXML
    public Label playerNameLabel;

    public Label playerScore;

    public void getPlayerScore(String messageScore) {
        playerScore.setText(messageScore);
    }

    public void getPlayerName(String message) {
        playerNameLabel.setText(message);
    }

    public void loadGameOverScene(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(new File("src/main/resources/GameOverMenu.fxml").toURI().toURL());
        Scene scene = anchorRoot.getScene();
        //Set Y of second scene to Height of window
        root.translateYProperty().set(scene.getHeight());
        //Add second scene. Now both first and second scene is present
        parentContainer.getChildren().add(root);

        //Create new TimeLine animation
        Timeline timeline = PlayerTimeLine.getTimeline();
        //Animate Y property
        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
        KeyFrame kf = new KeyFrame(Duration.millis(0.1), kv);
        timeline.getKeyFrames().add(kf);
        //After completing animation, remove first scene
        timeline.setOnFinished(t -> parentContainer.getChildren().remove(anchorRoot));
        timeline.play();
    }

    public void openMyLink(MouseEvent mouseEvent) {
        openWebpage("https://gitlab.com/users/Bremus");
    }

    public void openLinkColleague(MouseEvent mouseEvent) {
        openWebpage("https://gitlab.com/Val3nt1n");
    }

    public static void openWebpage(String url) {
        try {
            Desktop.getDesktop().browse(new URL(url).toURI());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void openLinkTrainer(MouseEvent mouseEvent) {
        openWebpage("https://gitlab.com/sda-international/users/courses/romania/javagalati1/javagalati1/gabriel_mototolea");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void onClickGoToMainMenu(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(new File("src/main/resources/MainMenu.fxml").toURI().toURL());
        Scene scene = anchorRoot.getScene();
        //Set Y of second scene to Height of window
        root.translateYProperty().set(scene.getHeight());
        //Add second scene. Now both first and second scene is present
        parentContainer.getChildren().add(root);

        //Create new TimeLine animation
        Timeline timeline = PlayerTimeLine.getTimeline();
        //Animate Y property
        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
        KeyFrame kf = new KeyFrame(Duration.millis(0.1), kv);
        timeline.getKeyFrames().add(kf);
        //After completing animation, remove first scene
        timeline.setOnFinished(t -> parentContainer.getChildren().remove(anchorRoot));
        timeline.play();
    }

    public void showHighScore(ActionEvent actionEvent) throws IOException {

        Parent root = FXMLLoader.load(new File("src/main/resources/HighScore.fxml").toURI().toURL());
        Scene scene = anchorRoot.getScene();
        //Set Y of second scene to Height of window
        root.translateYProperty().set(scene.getHeight());
        //Add second scene. Now both first and second scene is present
        parentContainer.getChildren().add(root);

        //Create new TimeLine animation
        Timeline timeline = PlayerTimeLine.getTimeline();
        //Animate Y property
        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
        KeyFrame kf = new KeyFrame(Duration.millis(0.1), kv);
        timeline.getKeyFrames().add(kf);
        //After completing animation, remove first scene
        timeline.setOnFinished(t -> parentContainer.getChildren().remove(anchorRoot));
        timeline.play();
    }

    public void onClickExit(ActionEvent actionEvent) {
        System.exit(0);
    }
}