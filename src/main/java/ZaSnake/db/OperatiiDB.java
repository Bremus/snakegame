package ZaSnake.db;

import ZaSnake.Player;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class OperatiiDB {
    private static final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public static Long insert(String name) {

        Transaction transaction = null;

        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            final Serializable identifier = session.save(new Player(null, name, null));
            System.out.println("Identifier for saved player: " + identifier);
            Long id = (Long) identifier;
            System.out.println("Id of inserted player: " + id);
            transaction.commit();
            return id;
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
            return null;
        }
    }

    public static void insertPlayer(String name, Integer score) {

        Transaction transaction = null;

        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(new Player(null, name, score));
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }

    public static List<Player> getPlayerDetails() {
        try (Session session = sessionFactory.openSession()) {
            Query<Player> queryUsers = session.createNamedQuery("getPlayersORderedByScore", Player.class);
            return queryUsers.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}