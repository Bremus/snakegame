package ZaSnake;

import javafx.animation.Timeline;

public class PlayerTimeLine {

    private static Long playerId;
    private static String playerName;
    private static Timeline timeline;
    private static Integer snakeLength = 1;

    private PlayerTimeLine() {}

    public static Long getPlayerId() {
        return playerId;
    }

    public static void setPlayerId(Long playerId) {
        PlayerTimeLine.playerId = playerId;
    }

    public static String getPlayerName() {
        return playerName;
    }

    public static void setPlayerName(String playerName) {
        PlayerTimeLine.playerName = playerName;
    }

    public static Timeline getTimeline() {
        return timeline;
    }

    public static void setTimeline(Timeline timeline) {
        PlayerTimeLine.timeline = timeline;
    }

    public static Integer getSnakeLength() {
        return snakeLength;
    }

    public static void setSnakeLength(Integer snakeLength) {
        PlayerTimeLine.snakeLength = snakeLength;
    }

}
